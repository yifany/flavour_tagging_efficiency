#include <iostream>
#include <fstream>
#include <vector>

class EffMap {

    std::string m_Name;
    std::vector<float> m_ptBins;
    int m_nFiles;

    TFile *m_rootfileOut;
    TEfficiency *m_BBEffCalibrated, *m_BTCEffCalibrated, *m_BLCEffCalibrated, *m_BLEffCalibrated;
    TEfficiency *m_CBEffCalibrated, *m_CTCEffCalibrated, *m_CLCEffCalibrated, *m_CLEffCalibrated;
    TEfficiency *m_LBEffCalibrated, *m_LTCEffCalibrated, *m_LLCEffCalibrated, *m_LLEffCalibrated;
    TEfficiency *m_TBEffCalibrated, *m_TTCEffCalibrated, *m_TLCEffCalibrated, *m_TLEffCalibrated;
    TEfficiency *m_BBEffFinely, *m_BTCEffFinely, *m_BLCEffFinely, *m_BLEffFinely;
    TEfficiency *m_CBEffFinely, *m_CTCEffFinely, *m_CLCEffFinely, *m_CLEffFinely;
    TEfficiency *m_LBEffFinely, *m_LTCEffFinely, *m_LLCEffFinely, *m_LLEffFinely;
    TEfficiency *m_TBEffFinely, *m_TTCEffFinely, *m_TLCEffFinely, *m_TLEffFinely;

    public:
    EffMap( std::string mcName );
    // void addJet( JetTag jet );
    void readFile( std::string fileName );
    void output();
    void addJet( float pt, float eta, float pb, float pc, float pu, int hadronID, float weight );

};

void drawEff( int j, float x[], float y1[], float y2[], float y3[], std::string taggerName );