#include "EffMap.cxx"
#include <iostream>
#include <fstream>

void runFileList( std::string name ){

    EffMap *effMap = new EffMap( name );

    std::ifstream sampleList;
    sampleList.open( name + ".txt" , std::ios::in );
    std::string sample;
    while( std::getline( sampleList, sample ) ) effMap->readFile( sample );
    
    effMap->output();

}