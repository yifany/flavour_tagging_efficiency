#include "EffMap.h"
#include <math.h>


EffMap::EffMap( std::string Name ) {
    m_Name = Name;
    // m_jets.clear();
    m_rootfileOut = new TFile( (m_Name + ".root").c_str(), "recreate" );

    const int nBinsPtCalibratedB = 9;
    double binsPtCalibratedB[ nBinsPtCalibratedB + 1 ] = {20.0,30.0,40.0,60.0,85.0,110.0,140.0,175.0,250.0,400.0};
    const int nBinsEtaCalibratedB = 1;
    double binsEtaCalibratedB[ nBinsEtaCalibratedB + 1 ] = {0.0,2.5};
    m_BBEffCalibrated = new TEfficiency( "CalibratedBB", "B_jet_B_efficiency_calibrated", nBinsPtCalibratedB, binsPtCalibratedB, nBinsEtaCalibratedB, binsEtaCalibratedB );
    m_BTCEffCalibrated = new TEfficiency( "CalibratedBTC", "B_jet_Ctight_efficiency_calibrated", nBinsPtCalibratedB, binsPtCalibratedB, nBinsEtaCalibratedB, binsEtaCalibratedB );
    m_BLCEffCalibrated = new TEfficiency( "CalibratedBLC", "B_jet_Cloose_efficiency_calibrated", nBinsPtCalibratedB, binsPtCalibratedB, nBinsEtaCalibratedB, binsEtaCalibratedB );
    m_BLEffCalibrated = new TEfficiency( "CalibratedBL", "B_jet_L_efficiency_calibrated", nBinsPtCalibratedB, binsPtCalibratedB, nBinsEtaCalibratedB, binsEtaCalibratedB );

    const int nBinsPtCalibratedC = 4;
    double binsPtCalibratedC[ nBinsPtCalibratedC + 1 ] = {20.0,40.0,65.0,140.0,250.0};
    const int nBinsEtaCalibratedC = 1;
    double binsEtaCalibratedC[ nBinsEtaCalibratedC + 1 ] = {0.0,2.5};
    m_CBEffCalibrated = new TEfficiency( "CalibratedCB", "C_jet_B_efficiency_calibrated", nBinsPtCalibratedC, binsPtCalibratedC, nBinsEtaCalibratedC, binsEtaCalibratedC );
    m_CTCEffCalibrated = new TEfficiency( "CalibratedCTC", "C_jet_Ctight_efficiency_calibrated", nBinsPtCalibratedC, binsPtCalibratedC, nBinsEtaCalibratedC, binsEtaCalibratedC );
    m_CLCEffCalibrated = new TEfficiency( "CalibratedCLC", "C_jet_Cloose_efficiency_calibrated", nBinsPtCalibratedC, binsPtCalibratedC, nBinsEtaCalibratedC, binsEtaCalibratedC );
    m_CLEffCalibrated = new TEfficiency( "CalibratedCL", "C_jet_L_efficiency_calibrated", nBinsPtCalibratedC, binsPtCalibratedC, nBinsEtaCalibratedC, binsEtaCalibratedC );

    const int nBinsPtCalibratedL = 4;
    double binsPtCalibratedL[ nBinsPtCalibratedL + 1 ] = {20.0,50.0,100.0,150.0,300.0};
    const int nBinsEtaCalibratedL = 1;
    double binsEtaCalibratedL[ nBinsEtaCalibratedL + 1 ] = {0.0,2.5};
    m_LBEffCalibrated = new TEfficiency( "CalibratedLB", "L_jet_B_efficiency_calibrated", nBinsPtCalibratedL, binsPtCalibratedL, nBinsEtaCalibratedL, binsEtaCalibratedL );
    m_LTCEffCalibrated = new TEfficiency( "CalibratedLTC", "L_jet_Ctight_efficiency_calibrated", nBinsPtCalibratedL, binsPtCalibratedL, nBinsEtaCalibratedL, binsEtaCalibratedL );
    m_LLCEffCalibrated = new TEfficiency( "CalibratedLLC", "L_jet_Cloose_efficiency_calibrated", nBinsPtCalibratedL, binsPtCalibratedL, nBinsEtaCalibratedL, binsEtaCalibratedL );
    m_LLEffCalibrated = new TEfficiency( "CalibratedLL", "L_jet_L_efficiency_calibrated", nBinsPtCalibratedL, binsPtCalibratedL, nBinsEtaCalibratedL, binsEtaCalibratedL );

    const int nBinsPtCalibratedT = 4;
    double binsPtCalibratedT[ nBinsPtCalibratedT + 1 ] = {20.0,40.0,65.0,140.0,250.0};
    const int nBinsEtaCalibratedT = 1;
    double binsEtaCalibratedT[ nBinsEtaCalibratedT + 1 ] = {0.0,2.5};
    m_TBEffCalibrated = new TEfficiency( "CalibratedTB", "T_jet_B_efficiency_calibrated", nBinsPtCalibratedT, binsPtCalibratedT, nBinsEtaCalibratedT, binsEtaCalibratedT );
    m_TTCEffCalibrated = new TEfficiency( "CalibratedTTC", "T_jet_Ctight_efficiency_calibrated", nBinsPtCalibratedT, binsPtCalibratedT, nBinsEtaCalibratedT, binsEtaCalibratedT );
    m_TLCEffCalibrated = new TEfficiency( "CalibratedTLC", "T_jet_Cloose_efficiency_calibrated", nBinsPtCalibratedT, binsPtCalibratedT, nBinsEtaCalibratedT, binsEtaCalibratedT );
    m_TLEffCalibrated = new TEfficiency( "CalibratedTL", "T_jet_L_efficiency_calibrated", nBinsPtCalibratedT, binsPtCalibratedT, nBinsEtaCalibratedT, binsEtaCalibratedT );

    const int nBinsPtFinelyB = 9;
    double binsPtFinelyB[ nBinsPtFinelyB + 1 ] = {20.0,30.0,40.0,60.0,85.0,110.0,140.0,175.0,250.0,400.0};
    const int nBinsEtaFinelyB = 4;
    double binsEtaFinelyB[ nBinsEtaFinelyB + 1 ] = {0, 0.6, 1.2, 1.8, 2.5};
    m_BBEffFinely = new TEfficiency( "FinelyBB", "B_jet_B_efficiency_finely", nBinsPtFinelyB, binsPtFinelyB, nBinsEtaFinelyB, binsEtaFinelyB );
    m_BTCEffFinely = new TEfficiency( "FinelyBTC", "B_jet_Ctight_efficiency_finely", nBinsPtFinelyB, binsPtFinelyB, nBinsEtaFinelyB, binsEtaFinelyB );
    m_BLCEffFinely = new TEfficiency( "FinelyBLC", "B_jet_Cloose_efficiency_finely", nBinsPtFinelyB, binsPtFinelyB, nBinsEtaFinelyB, binsEtaFinelyB );
    m_BLEffFinely = new TEfficiency( "FinelyBL", "B_jet_L_efficiency_finely", nBinsPtFinelyB, binsPtFinelyB, nBinsEtaFinelyB, binsEtaFinelyB );

    const int nBinsPtFinelyC = 4;
    double binsPtFinelyC[ nBinsPtFinelyC + 1 ] = {20.0,40.0,65.0,140.0,250.0};
    const int nBinsEtaFinelyC = 4;
    double binsEtaFinelyC[ nBinsEtaFinelyC + 1 ] = {0, 0.6, 1.2, 1.8, 2.5};
    m_CBEffFinely = new TEfficiency( "FinelyCB", "C_jet_B_efficiency_Finely", nBinsPtFinelyC, binsPtFinelyC, nBinsEtaFinelyC, binsEtaFinelyC );
    m_CTCEffFinely = new TEfficiency( "FinelyCTC", "C_jet_Ctight_efficiency_Finely", nBinsPtFinelyC, binsPtFinelyC, nBinsEtaFinelyC, binsEtaFinelyC );
    m_CLCEffFinely = new TEfficiency( "FinelyCLC", "C_jet_Cloose_efficiency_Finely", nBinsPtFinelyC, binsPtFinelyC, nBinsEtaFinelyC, binsEtaFinelyC );
    m_CLEffFinely = new TEfficiency( "FinelyCL", "C_jet_L_efficiency_Finely", nBinsPtFinelyC, binsPtFinelyC, nBinsEtaFinelyC, binsEtaFinelyC );

    const int nBinsPtFinelyL = 4;
    double binsPtFinelyL[ nBinsPtFinelyL + 1 ] = {20.0,50.0,100.0,150.0,300.0};
    const int nBinsEtaFinelyL = 4;
    double binsEtaFinelyL[ nBinsEtaFinelyL + 1 ] = {0, 0.6, 1.2, 1.8, 2.5};
    m_LBEffFinely = new TEfficiency( "FinelyLB", "L_jet_B_efficiency_Finely", nBinsPtFinelyL, binsPtFinelyL, nBinsEtaFinelyL, binsEtaFinelyL );
    m_LTCEffFinely = new TEfficiency( "FinelyLTC", "L_jet_Ctight_efficiency_Finely", nBinsPtFinelyL, binsPtFinelyL, nBinsEtaFinelyL, binsEtaFinelyL );
    m_LLCEffFinely = new TEfficiency( "FinelyLLC", "L_jet_Cloose_efficiency_Finely", nBinsPtFinelyL, binsPtFinelyL, nBinsEtaFinelyL, binsEtaFinelyL );
    m_LLEffFinely = new TEfficiency( "FinelyLL", "L_jet_L_efficiency_Finely", nBinsPtFinelyL, binsPtFinelyL, nBinsEtaFinelyL, binsEtaFinelyL );

    const int nBinsPtFinelyT = 4;
    double binsPtFinelyT[ nBinsPtFinelyT + 1 ] = {20.0,40.0,65.0,140.0,250.0};
    const int nBinsEtaFinelyT = 4;
    double binsEtaFinelyT[ nBinsEtaFinelyT + 1 ] = {0, 0.6, 1.2, 1.8, 2.5};
    m_TBEffFinely = new TEfficiency( "FinelyTB", "T_jet_B_efficiency_Finely", nBinsPtFinelyT, binsPtFinelyT, nBinsEtaFinelyT, binsEtaFinelyT );
    m_TTCEffFinely = new TEfficiency( "FinelyTTC", "T_jet_Ctight_efficiency_Finely", nBinsPtFinelyT, binsPtFinelyT, nBinsEtaFinelyT, binsEtaFinelyT );
    m_TLCEffFinely = new TEfficiency( "FinelyTLC", "T_jet_Cloose_efficiency_Finely", nBinsPtFinelyT, binsPtFinelyT, nBinsEtaFinelyT, binsEtaFinelyT );
    m_TLEffFinely = new TEfficiency( "FinelyTL", "T_jet_L_efficiency_Finely", nBinsPtFinelyT, binsPtFinelyT, nBinsEtaFinelyT, binsEtaFinelyT );


    m_nFiles = 0;
}



void EffMap::readFile( std::string fileName ) {
    TFile* inputFile = TFile::Open( fileName.c_str() );
    TTree* inputTree = nullptr;
    inputFile->GetObject( "nominal", inputTree );
    m_nFiles++;
    std::cout << fileName << " " << m_nFiles << "\n" ;

    std::vector<float> *pb = 0;
    std::vector<float> *pc = 0;
    std::vector<float> *pu = 0;
    std::vector<float> *pt = 0;
    std::vector<float> *eta = 0;
    std::vector<float> *jvt = 0;
    std::vector<int> *hadronConeID = 0;
    size_t i, j, m, n;
    float weight, weightMc, weightPileUp;


    inputTree->SetBranchAddress( "weight_mc", &weightMc );
    inputTree->SetBranchAddress( "weight_pileup", &weightPileUp );
    inputTree->SetBranchAddress( "jet_pt", &pt );
    inputTree->SetBranchAddress( "jet_eta", &eta );
    inputTree->SetBranchAddress( "jet_jvt", &jvt );
    inputTree->SetBranchAddress( "HadronConeExclTruthLabelID", &hadronConeID );

    if( inputTree->GetListOfBranches()->FindObject("jet_DL1r_pu") ) {
        inputTree->SetBranchAddress( "jet_DL1r_pu", &pu );
        inputTree->SetBranchAddress( "jet_DL1r_pc", &pc );
        inputTree->SetBranchAddress( "jet_DL1r_pb", &pb );
    }
    else if( inputTree->GetListOfBranches()->FindObject("DL1r_pu") ) {
        inputTree->SetBranchAddress( "DL1r_pu", &pu );
        inputTree->SetBranchAddress( "DL1r_pc", &pc );
        inputTree->SetBranchAddress( "DL1r_pb", &pb );
    }
    else {
        std::cout << "No DL1r information found !!!!!!!!!!!!\n";
        return;
    }


    j = inputTree->GetEntries();

    for( i = 0; i < j; i++ ){

        
        inputTree->GetEntry( i );
        weight = weightMc * weightPileUp ;
        m = pt->size();
        for( n = 0; n < m; n++ ){
            if ( pt->at(n) < 60000. && abs( eta->at(n) ) < 2.4 && jvt->at(n) < 0.59 ) continue;
            addJet( pt->at(n) / 1000. , abs( eta->at(n) ), pb->at(n), pc->at(n), pu->at(n), hadronConeID->at(n), weight );
        }
    }
    inputFile->Close();
}

void EffMap::addJet( float pt, float eta, float pb, float pc, float pu, int hadronID, float weight ) {
    float disB, disC;
    disB = log( pb / ( 0.018 * pc + 0.982 * pu ) );
    disC = log( pc / ( 0.3 * pb + 0.7 * pu ) );
    if ( hadronID == 0 ) {
        if ( disB > 3.245 ) {
            m_LBEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_LTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LBEffFinely->FillWeighted( true, weight, pt, eta);
            m_LTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_LLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_LLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else if( disC > 1.76 ) {
            m_LBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LTCEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_LLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LBEffFinely->FillWeighted( false, weight, pt, eta);
            m_LTCEffFinely->FillWeighted( true, weight, pt, eta);
            m_LLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_LLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else if( disC > 0.11 ) {
            m_LBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LLCEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_LLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LBEffFinely->FillWeighted( false, weight, pt, eta);
            m_LTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_LLCEffFinely->FillWeighted( true, weight, pt, eta);
            m_LLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else  {
            m_LBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_LLEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_LBEffFinely->FillWeighted( false, weight, pt, eta);
            m_LTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_LLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_LLEffFinely->FillWeighted( true, weight, pt, eta);
        }
    }
    if ( hadronID == 4 ) {
        if ( disB > 3.245 ) {
            m_CBEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_CTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CBEffFinely->FillWeighted( true, weight, pt, eta);
            m_CTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_CLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_CLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else if( disC > 1.76 ) {
            m_CBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CTCEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_CLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CBEffFinely->FillWeighted( false, weight, pt, eta);
            m_CTCEffFinely->FillWeighted( true, weight, pt, eta);
            m_CLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_CLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else if( disC > 0.11 ) {
            m_CBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CLCEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_CLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CBEffFinely->FillWeighted( false, weight, pt, eta);
            m_CTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_CLCEffFinely->FillWeighted( true, weight, pt, eta);
            m_CLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else  {
            m_CBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_CLEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_CBEffFinely->FillWeighted( false, weight, pt, eta);
            m_CTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_CLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_CLEffFinely->FillWeighted( true, weight, pt, eta);
        }
    }
    if ( hadronID == 5 ) {
        if ( disB > 3.245 ) {
            m_BBEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_BTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BBEffFinely->FillWeighted( true, weight, pt, eta);
            m_BTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_BLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_BLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else if( disC > 1.76 ) {
            m_BBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BTCEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_BLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BBEffFinely->FillWeighted( false, weight, pt, eta);
            m_BTCEffFinely->FillWeighted( true, weight, pt, eta);
            m_BLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_BLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else if( disC > 0.11 ) {
            m_BBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BLCEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_BLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BBEffFinely->FillWeighted( false, weight, pt, eta);
            m_BTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_BLCEffFinely->FillWeighted( true, weight, pt, eta);
            m_BLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else  {
            m_BBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_BLEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_BBEffFinely->FillWeighted( false, weight, pt, eta);
            m_BTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_BLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_BLEffFinely->FillWeighted( true, weight, pt, eta);
        }
    }
    if ( hadronID == 15 ) {
        if ( disB > 3.245 ) {
            m_TBEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_TTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TBEffFinely->FillWeighted( true, weight, pt, eta);
            m_TTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_TLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_TLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else if( disC > 1.76 ) {
            m_TBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TTCEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_TLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TBEffFinely->FillWeighted( false, weight, pt, eta);
            m_TTCEffFinely->FillWeighted( true, weight, pt, eta);
            m_TLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_TLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else if( disC > 0.11 ) {
            m_TBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TLCEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_TLEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TBEffFinely->FillWeighted( false, weight, pt, eta);
            m_TTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_TLCEffFinely->FillWeighted( true, weight, pt, eta);
            m_TLEffFinely->FillWeighted( false, weight, pt, eta);
        }
        else  {
            m_TBEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TTCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TLCEffCalibrated->FillWeighted( false, weight, pt, eta);
            m_TLEffCalibrated->FillWeighted( true, weight, pt, eta);
            m_TBEffFinely->FillWeighted( false, weight, pt, eta);
            m_TTCEffFinely->FillWeighted( false, weight, pt, eta);
            m_TLCEffFinely->FillWeighted( false, weight, pt, eta);
            m_TLEffFinely->FillWeighted( true, weight, pt, eta);
        }
    }
}

void EffMap::output(){

    m_rootfileOut->Write();

    m_rootfileOut->Close();
}

